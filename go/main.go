// go/main.go
package main

import (
    "fmt"
    "os"
    "os/exec"
    "path/filepath"
    "strconv"
)

func startPod(version int) {
    if version < 10 {
        fmt.Println("Only postgres >= 10")
        os.Exit(1)
    }

    xdgDir := os.Getenv("XDG_CONFIG_HOME")
    if xdgDir == "" {
        homeDir, err := os.UserHomeDir()
        if err != nil {
            fmt.Println("Error: XDG_CONFIG_HOME is not set or not a directory.")
            os.Exit(1)
        }
        xdgDir = filepath.Join(homeDir, ".config")
    }

    confDir := filepath.Join(xdgDir, "pgpod")
    if err := os.MkdirAll(confDir, 0755); err != nil {
        fmt.Println("Error creating config directory:", err)
        os.Exit(1)
    }

    confFile := filepath.Join(confDir, fmt.Sprintf("%d.conf", version))
    if _, err := os.Stat(confFile); os.IsNotExist(err) {
        fmt.Println("Error: Conf file does not exist. Creating", confFile)
        if err := os.WriteFile(confFile, []byte{}, 0600); err != nil {
            fmt.Println("Error creating conf file:", err)
            os.Exit(1)
        }
    }

    content, err := os.ReadFile(confFile)
    if err != nil {
        fmt.Println("Error reading conf file:", err)
        os.Exit(1)
    }

    if !contains(string(content), "POSTGRES_USER") || !contains(string(content), "POSTGRES_PASSWORD") {
        fmt.Println("POSTGRES_USER or POSTGRES_PASSWORD not found. Adding default values.")
        password := exec.Command("openssl", "rand", "-base64", "12").Output()
        if err != nil {
            fmt.Println("Error generating password:", err)
            os.Exit(1)
        }
        newContent := fmt.Sprintf("POSTGRES_USER=postgres\nPOSTGRES_PASSWORD=%s\n", string(password))
        if err := os.WriteFile(confFile, []byte(newContent), 0600); err != nil {
            fmt.Println("Error writing to conf file:", err)
            os.Exit(1)
        }
    }

    containerName := fmt.Sprintf("postgres%d", version)
    volumeName := fmt.Sprintf("postgres%d", version)
    externalPort := 5400 + version
    internalPort := 5432
    pgdata := "/var/lib/postgresql/data/pg"

    if err := exec.Command("podman", "volume", "inspect", volumeName).Run(); err != nil {
        if err := exec.Command("podman", "volume", "create", volumeName).Run(); err != nil {
            fmt.Println("Error creating volume:", err)
            os.Exit(1)
        }
    }

    cmd := exec.Command("podman", "run", "-d",
        "--name", containerName,
        "--env-file", confFile,
        "-e", fmt.Sprintf("PGDATA=%s", pgdata),
        "-p", fmt.Sprintf("%d:%d", externalPort, internalPort),
        "--user", "1000:1000",
        "-v", fmt.Sprintf("%s:/var/lib/postgresql/data", volumeName),
        "--pull", "always",
        fmt.Sprintf("postgres:%d-bookworm", version),
    )
    if err := cmd.Run(); err != nil {
        fmt.Println("Error starting container:", err)
        os.Exit(1)
    }

    fmt.Println(containerName, "- started successfully.")
}

func stopPod(version int) {
    if version < 10 {
        fmt.Println("Only postgres >= 10")
        os.Exit(1)
    }

    containerName := fmt.Sprintf("postgres%d", version)
    if err := exec.Command("podman", "stop", containerName).Run(); err != nil {
        fmt.Println("Error stopping container:", err)
        os.Exit(1)
    }
    if err := exec.Command("podman", "rm", containerName).Run(); err != nil {
        fmt.Println("Error removing container:", err)
        os.Exit(1)
    }
    fmt.Println(containerName, "- stopped and removed successfully.")
}

func contains(s, substr string) bool {
    return strings.Contains(s, substr)
}

func main() {
    if len(os.Args) < 3 {
        fmt.Println("Usage: go run main.go {start|down} <postgres_version>")
        os.Exit(1)
    }

    action := os.Args[1]
    version, err := strconv.Atoi(os.Args[2])
    if err != nil {
        fmt.Println("Invalid version:", os.Args[2])
        os.Exit(1)
    }

    switch action {
    case "start":
        startPod(version)
    case "down":
        stopPod(version)
    default:
        fmt.Println("Usage: go run main.go {start|down} <postgres_version>")
        os.Exit(1)
    }
}
